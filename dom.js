let corpo = document.querySelector('.minhaLista');
let submit = document.querySelector('#submit');
submit.addEventListener('click', ()=>{
    var msg = document.getElementById("texto").value;
    const novaMsg = document.createElement("p");
    novaMsg.innerText = msg;
    const nova_div = document.createElement('div');
    nova_div.classList.add('div');
    novaMsg.classList.add('p');
    let botaoEditar = document.createElement('button');
    let botaoExcluir = document.createElement('button');
    botaoExcluir.classList.add('botaoExcluir');
    botaoEditar.classList.add('botaoEditar');
    botaoEditar.innerText = "Editar";
    botaoExcluir.innerText = "Excluir";
    nova_div.insertAdjacentElement("afterbegin",botaoExcluir);
    nova_div.insertAdjacentElement("afterbegin",botaoEditar);
    nova_div.insertAdjacentElement("afterbegin",novaMsg);
    corpo.appendChild(nova_div);
    
    const novaArea = document.getElementById('texto');
    novaArea.value = ''
    
    botaoExcluir.addEventListener('click', ()=>{
        event.target.parentNode.remove();
    });

});
